/**********************  DMDD_Group_4 CREATE TABLE STATEMENTS  *******************/

USE "DMDD_Group_4";

DROP TABLE IF Exists Orders;
DROP TABLE IF Exists OrderStatuses;
DROP TABLE IF Exists ManufacturerInventory;
DROP TABLE IF Exists VaccineInventory;
DROP TABLE IF Exists VaccinationCenterWarehouse;
DROP TABLE IF Exists VaccinationBooking;
DROP TABLE IF Exists VaccinationSlot;
DROP TABLE IF Exists Vaccine;
DROP TABLE IF Exists Manufacturer;
DROP TABLE IF Exists Doctor;
DROP TABLE IF Exists VaccinationCenter;
DROP TABLE IF Exists Feedback;
DROP TABLE IF Exists Disease;
DROP TABLE IF Exists Patient;

CREATE TABLE dbo.Patient
    ( 
    PatientID INT NOT NULL PRIMARY KEY , 
    PatientFirstName VARCHAR(40),
	PatientLastName VARCHAR(40),
	PatientEmailID VARCHAR(50),
	PatientPhoneNumber VARCHAR(50),
	PatientBloodGroup VARCHAR(3),
	PatientGender VARCHAR(10),
	PatientSSN VARCHAR(20),
	PatientDateOfBirth DATE,
	PatientAge AS DATEDIFF(hour,PatientDateOfBirth,GETDATE())/8766,
	PatientInsuranceProvider VARCHAR(100),
	PatientInsuranceNumber VARCHAR(45),
	PatientStreet VARCHAR(50),
	PatientCity VARCHAR(45),
	PatientState VARCHAR(45),
	PatientCountry VARCHAR(45),
	PatientZipCode VARCHAR(10)
    );

CREATE TABLE dbo.Disease
	(
	DiseaseID INT NOT NULL PRIMARY KEY,
	DiseaseName VARCHAR(50)
	);

CREATE TABLE dbo.Feedback
	(
	FeedbackID INT NOT NULL PRIMARY KEY,
	Comments TEXT,
	FeedbackDateTime DATETIME DEFAULT Current_Timestamp,
	PatientID INT NOT NULL
		REFERENCES Patient(PatientID)
	);

CREATE TABLE dbo.VaccinationCenter
    ( 
    VaccinationCenterID INT NOT NULL PRIMARY KEY,
	VaccinationCenterType VARCHAR(45),
	VaccinationCenterName VARCHAR(45),
	VaccinationCenterStreet VARCHAR(45),
	VaccinationCenterCity VARCHAR(45),
	VaccinationCenterState VARCHAR(45),
	VaccinationCenterCountry VARCHAR(45),
	VaccinationCenterZipCode VARCHAR(10),
	VaccinationCenterPhoneNumber VARCHAR(45)
    );

CREATE TABLE dbo.Doctor 
     ( 
    DoctorID INT NOT NULL PRIMARY KEY, 
    DoctorFirstName VARCHAR(45), 
    DoctorLastName VARCHAR(45),
	DoctorRegistrationNumber VARCHAR(30),
	DoctorSpecialization VARCHAR(45),
	DoctorPhoneNumber VARCHAR(50),
	DoctorsEmailID VARCHAR(50),
	VaccinationCenterID INT NOT NULL
		REFERENCES VaccinationCenter(VaccinationCenterID)
    );

CREATE TABLE dbo.Manufacturer
	(
	ManufacturerID INT NOT NULL PRIMARY KEY,
	ManufacturerName VARCHAR(45),
	ManufacturerStreet VARCHAR(45),
	ManufacturerCity VARCHAR(45),
	ManufacturerState VARCHAR(45),
	ManufacturerCountry VARCHAR(45),
	ManufacturerZipCode VARCHAR(10)
	);

CREATE TABLE dbo.Vaccine
	(
	VaccineID INT NOT NULL PRIMARY KEY,
	VaccineName VARCHAR(50),
	ManufacturerID INT NOT NULL
		REFERENCES Manufacturer(ManufacturerID),
	Cost DECIMAL(8,3),
	DiseaseID INT NOT NULL
		REFERENCES Disease(DiseaseID)
	);

CREATE TABLE dbo.VaccinationSlot
     ( 
    VaccinationSlotID INT NOT NULL PRIMARY KEY, 
    VaccinationSlotDateTime DATETIME,
	VaccinationCenterID INT NOT NULL
        REFERENCES VaccinationCenter(VaccinationCenterID), 
	DoctorID INT NOT NULL
		REFERENCES Doctor(DoctorID),
    Reserved TINYINT,
    );

CREATE TABLE dbo.VaccinationBooking
	(
	BookingID INT NOT NULL PRIMARY KEY,
	PatientID INT NOT NULL
		REFERENCES Patient(PatientID),
	BookingRequestDate DATETIME DEFAULT Current_Timestamp,
	VaccinationDate DATETIME,
	VaccinationSlotID INT NOT NULL
		REFERENCES VaccinationSlot(VaccinationSlotID),
	VaccineID INT NOT NULL
		REFERENCES Vaccine(VaccineID)
	);

CREATE TABLE dbo.VaccinationCenterWarehouse
	(
	VaccinationCenterWarehouseID INT NOT NULL PRIMARY KEY,
	VaccinationCenterID INT NOT NULL
		REFERENCES VaccinationCenter(VaccinationCenterID),
	VaccinationCenterWarehouseStreet VARCHAR(45),
	VaccinationCenterWarehouseCity VARCHAR(45),
	VaccinationCenterWarehouseState VARCHAR(45),
	VaccinationCenterWarehouseCount VARCHAR(45),
	VaccinationCenterWarehouseZipcode VARCHAR(45)
	);

CREATE TABLE dbo.VaccineInventory
	(
	 VaccinationCenterWarehouseID INT NOT NULL
		REFERENCES VaccinationCenterWarehouse(VaccinationCenterWarehouseID),
	 VaccineID INT NOT NULL
		REFERENCES Vaccine(VaccineID),
	 BatchID INT NOT NULL PRIMARY KEY,
	 VaccineManufacturingDate DATETIME,
	 VaccineExpiryDate DATETIME,
	 VaccineCount INT
	);

CREATE TABLE dbo.ManufacturerInventory
	(
	ManufacturerInventoryID INT NOT NULL PRIMARY KEY,
	VaccineID INT NOT NULL
		REFERENCES Vaccine(VaccineID),
	VaccineQuantity INT,
	ManufacturerID INT NOT NULL
		REFERENCES Manufacturer(ManufacturerID)
	);

CREATE TABLE dbo.OrderStatuses 
	(
	OrderStatusID int PRIMARY KEY, 
	OrderStatus VARCHAR(30)
	);

CREATE TABLE dbo.Orders
    (
    OrderID INT NOT NULL PRIMARY KEY,
    OrderDate DATETIME DEFAULT Current_Timestamp,
    OrderDeliveryDate DATETIME,
    OrderQuantity INT,
    VaccinationCenterID INT NOT NULL
        REFERENCES VaccinationCenter(VaccinationCenterID),
    VaccineID INT NOT NULL
        REFERENCES Vaccine(VaccineID),
    ManufacturerID INT NOT NULL
        REFERENCES Manufacturer(ManufacturerID),
    OrderStatusID INT NOT NULL
    	REFERENCES OrderStatuses(OrderStatusID)
    );

--------------------------------------------------------------------------------------------------

