/*********  DMDD_Group_4 TABLE LEVEL CHECK CONSTRAINTS AND TRIGGERS FOR VACCINATION MANAGEMENT SYSTEM *********/


/* 1. A patient can not book an appointment for any previous dates that has been past 
 * with respect to the current date where he/she wants to make a booking
 * 2. A patient can not book an appointment for dates beyond 30 days in the future with respect to the current booking date(today) 
 * i.e If the current date is November 1st, he/she can not make a booking for December 1st and beyond  */

USE "DMDD_Group_4";
GO

CREATE TRIGGER bookingDateCheck
ON VaccinationBooking
AFTER INSERT AS 
BEGIN 
IF EXISTS  (SELECT BookingID, BookingRequestDate 
			FROM VaccinationBooking
			WHERE VaccinationDate > DATEADD(DAY, 30, BookingRequestDate) or VaccinationDate < BookingRequestDate)
	BEGIN		
		PRINT 'Please choose a vaccination slot within next 30 days only'
		ROLLBACK 
	END		
END;

GO

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/* 3. A patient can not book more than one vaccination appointment per day 
 * i.e If a patient books an appoinment for November 3rd, he can not have another booking on November 3rd */

CREATE TRIGGER appointmentDateCheck
ON VaccinationBooking
AFTER INSERT AS 
BEGIN 
IF EXISTS ( SELECT PatientID, vs.VaccinationSlotDateTime , COUNT(*) 
            FROM VaccinationBooking vb
            JOIN VaccinationSlot vs
            ON vb.VaccinationSlotID = vs.VaccinationSlotID
            GROUP BY PatientID, vs.VaccinationSlotDateTime
            HAVING COUNT(*) > 1
            )
    BEGIN        
        PRINT 'You can not have more than one appointment on a day'
        ROLLBACK 
    END        
END;

GO


---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/* 4. Patient can not book an already reserved vaccination slot i.e. If Patient1 has booked a slot for Nov 3rd, 10AM 
 * then another booking from Patient2 will not be successful  */

CREATE FUNCTION checkIfVacctionSlotIsReserved (@VSID int)
RETURNS smallint
AS
BEGIN
   DECLARE @Count smallint=0;
   SELECT @Count = COUNT(VaccinationSlotID) 
          FROM VaccinationSlot
          WHERE VaccinationSlotID  = @VSID
          AND Reserved = 1;
   RETURN @Count;
END;

GO

ALTER TABLE VaccinationBooking ADD CONSTRAINT checkForVaccineSlot CHECK 
(dbo.checkIfVacctionSlotIsReserved(VaccinationSlotID) = 0);


---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/* 5. Check if the requested vaccine id by the patient is available for at the slected vaccination centre 
 * .i.e If a patient reqests Vaccine X at vaccination center Y then we will check if the stock of the vaccine 
 * is avaiable in the vaccination center's warehouse before the patient can resever a slot */

GO
CREATE FUNCTION getWarehouseId(@vaccineCenterId int)
RETURNS INT
AS
	BEGIN
	    DECLARE @VaccinationCenterWarehouseID int = 0
	    SELECT @VaccinationCenterWarehouseID = VaccinationCenterWarehouseID
	    FROM VaccinationCenterWarehouse
	    WHERE VaccinationCenterID = @vaccineCenterId
	    RETURN @VaccinationCenterWarehouseID
	END

GO

CREATE FUNCTION getVaccineCenterId(@VaccinationSlotID int)
RETURNS INT
AS
BEGIN
        DECLARE @VaccinationCenterID int = 0
	    SELECT @VaccinationCenterID = VaccinationCenterID
	    FROM VaccinationSlot
	    WHERE VaccinationSlotID = @VaccinationSlotID
    RETURN @VaccinationCenterID
END
GO

CREATE FUNCTION checkVaccineAvailability(@vacID varchar(30), @VaccinationSlotID int)
RETURNS SMALLINT
AS
BEGIN
 	DECLARE @isAvailable SMALLINT  = 0;
    DECLARE @getCount VARCHAR(30);
    DECLARE @warehouseId int = 0;
    DECLARE @vaccineCenterId INT = 0;
    SET @vaccineCenterId = (SELECT dbo.getVaccineCenterId(@VaccinationSlotID))
	SET @warehouseId = (SELECT dbo.getWarehouseId(@vaccineCenterId))
	    SELECT @getCount = VaccineCount
	    FROM VaccineInventory
	    WHERE VaccineID = @vacID AND VaccinationCenterWarehouseID = @warehouseId
    BEGIN
    	IF @getCount <=1
    		BEGIN
    			SET @isAvailable = 0
    		END
    	ELSE
    		BEGIN
    			SET @isAvailable = 1
    		END
    END
    RETURN @isAvailable
END
GO


ALTER TABLE dbo.VaccinationBooking ADD CONSTRAINT checkForVaccCount check(dbo.checkVaccineAvailability(VaccineID, VaccinationSlotID)=1);

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/* 6. Check if the order can be fulfilled with the requested number of vaccine from the manufacturer 
 * i.e check if the manufacturer has enough number of vaccine for the requested vaccine */ 

INSERT INTO OrderStatuses 
VALUES
(1001, 'RECEIVED'),
(1002, 'PROCESSING'),
(1003, 'DELIVERED');
GO


CREATE FUNCTION dbo.orderQuantityCheck
(@OrderStatusID INT, @OrderQuantity INT, @VaccineID INT, @ManufacturerID INT)
RETURNS SMALLINT
AS
BEGIN
	IF @OrderStatusID != (SELECT OrderStatusID FROM OrderStatuses WHERE OrderStatus = 'DELIVERED')
		RETURN 0;
	
	DECLARE @ManufacturerVaccineQuantity INT = 0;

	SELECT @ManufacturerVaccineQuantity = VaccineQuantity 
	FROM dbo.ManufacturerInventory 
	WHERE VaccineID = @VaccineID AND ManufacturerID = @ManufacturerID;
	
	
	IF (@ManufacturerVaccineQuantity - @OrderQuantity) < 0
	BEGIN 
		RETURN 1;
	END
	RETURN 0;
END
GO


ALTER TABLE dbo.Orders ADD CONSTRAINT orderQuantityCheckConstraint 
	CHECK (dbo.orderQuantityCheck(OrderStatusID, OrderQuantity, VaccineID, ManufacturerID) = 0);
	
-----------------------------------------------------------------------------------------------------------------------------
