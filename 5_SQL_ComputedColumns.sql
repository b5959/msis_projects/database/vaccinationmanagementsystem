/*********  DMDD_Group_4 Computed Columns Statements  *********/

--This file contains computed columns for vaccination management system

/* List of computed Columns 
 1. Trigger to decremet the vaccineCount column in vaccineInventory table once the booking is recorded in the VaccinationBooking table.
 2. Trigger to increment the vaccineCount column in vaccineInventory table and decrement the VaccineQuantity column in	
    ManufacturerInventory table once the vaccination supply order moves to "DELIVERED" status.
 3. Trigger to book/cancel the vaccination slot using the reserved column in the vaccinationSlot table.
 4. Trigger to update the vaccinationDate in vaccinationBooking table.
 5. Calculate the PatientAge using the the PatientDateOfBirth.
 */

-------------------------------------------------------------------------------------------------------------------------------------------------
--1. Trigger to decremet the vaccineCount column in vaccineInventory table once the booking is recorded in the VaccinationBooking table.

USE "DMDD_Group_4";
GO

CREATE TRIGGER ut_vaccinationCountDec
ON dbo.VaccinationBooking 
FOR INSERT
AS
BEGIN
	DECLARE @vaccineID INT = 0;
	DECLARE @VaccinationSlotID INT = 0;

	DECLARE @VaccinationCenterID INT = 0; --get the VaccinationCenterID from VaccinationSlotID
	DECLARE @VaccinationCenterWarehouseID INT = 0; --get the VaccinationCenterWarehouseID from VaccinationCenterWarehouse using VaccinationCenterID
	DECLARE @VaccineCount INT = 0;
	
	SELECT @vaccineID = vaccineID FROM  inserted
	SELECT @VaccinationSlotID = VaccinationSlotID FROM  inserted 

	SELECT @VaccinationCenterID = VaccinationCenterID
	FROM dbo.VaccinationSlot vs  
	WHERE vs.VaccinationSlotID = @VaccinationSlotID
	
	SELECT @VaccinationCenterWarehouseID = VaccinationCenterWarehouseID
	FROM dbo.VaccinationCenterWarehouse vcw
	WHERE vcw.VaccinationCenterID = @VaccinationCenterID
	
	SELECT @VaccineCount = VaccineCount
	FROM dbo.VaccineInventory vi 
	WHERE VaccinationCenterWarehouseID = @VaccinationCenterWarehouseID AND VaccineID = @vaccineID
	
	UPDATE dbo.VaccineInventory 
	SET VaccineCount =  @VaccineCount - 1
	WHERE VaccinationCenterWarehouseID = @VaccinationCenterWarehouseID 
		AND VaccineID = @vaccineID 
END

-------------------------------------------------------------------------------------------------------------------------------------------------
/* 
 2. Trigger to increment the vaccineCount column in vaccineInventory table and decrement the VaccineQuantity column in	
 ManufacturerInventory table once order moves to "DELIVERED" status 
*/  


GO	
CREATE TRIGGER ut_vaccinationQuantityManagement
ON dbo.Orders
FOR INSERT, UPDATE 
AS
BEGIN
	DECLARE @VaccinationCenterID INT = 0;
	DECLARE @vaccineID INT = 0;
	DECLARE @ManufacturerID INT = 0;
	DECLARE @VaccinationCenterWarehouseID INT = 0;
	DECLARE @OrderQuantity INT = 0;
	DECLARE @OrderStatusID INT;
	DECLARE @OldOrderStatusID INT;
	DECLARE @DeliveredStatusID INT;
	
	SELECT @DeliveredStatusID=OrderStatusID  FROM OrderStatuses os WHERE OrderStatus = 'DELIVERED';
	SELECT @OrderStatusID = OrderStatusID FROM inserted;
	SELECT @OldOrderStatusID = ISNULL (OrderStatusID, 0) FROM deleted;
	
	if @OldOrderStatusID=@DeliveredStatusID
      Begin
          Return
      End
	if @OrderStatusID=@DeliveredStatusID
	  Begin
		SELECT @VaccinationCenterID = COALESCE(i.VaccinationCenterID, d.VaccinationCenterID) FROM inserted i FULL JOIN deleted d ON i.OrderID = d.OrderID 
		SELECT @VaccineID = COALESCE(i.VaccineID, d.VaccineID) FROM inserted i FULL JOIN deleted d ON i.OrderID = d.OrderID
		SELECT @ManufacturerID = COALESCE(i.ManufacturerID, d.ManufacturerID) FROM inserted i FULL JOIN deleted d ON i.OrderID = d.OrderID
		SELECT @OrderQuantity = COALESCE(i.OrderQuantity, d.OrderQuantity) FROM inserted i FULL JOIN deleted d ON i.OrderID = d.OrderID
		
		SELECT @VaccinationCenterWarehouseID = VaccinationCenterWarehouseID 
		FROM dbo.VaccinationCenterWarehouse vcw 
		WHERE VaccinationCenterID = @VaccinationCenterID
		
		UPDATE dbo.VaccineInventory 
		SET VaccineCount =  VaccineCount + @OrderQuantity
		WHERE VaccinationCenterWarehouseID = @VaccinationCenterWarehouseID 
			AND vaccineID = @vaccineID 
			
		UPDATE dbo.ManufacturerInventory  
		SET VaccineQuantity =  VaccineQuantity - @OrderQuantity
		WHERE ManufacturerInventoryID = (SELECT ManufacturerInventoryID FROM INSERTED) 
			AND vaccineID = @vaccineID
	  End
END


-------------------------------------------------------------------------------------------------------------------------------------------------
--3. Trigger to book/cancel the vaccination slot using the reserved column in the vaccinationSlot table

GO	
CREATE TRIGGER ut_UpdateReservedColumn
ON dbo.VaccinationBooking 
FOR INSERT, UPDATE, DELETE
AS
BEGIN
	DECLARE @VaccinationSlotID INT = 0;

	SELECT @VaccinationSlotID=COALESCE(i.VaccinationSlotID, d.VaccinationSlotID) FROM inserted i FULL JOIN deleted d ON i.BookingID = d.BookingID;
	
	IF (SELECT COUNT(VaccinationSlotID) FROM inserted) > 0
		BEGIN
			UPDATE dbo.VaccinationSlot 
			SET Reserved = 1
			WHERE VaccinationSlotID=@VaccinationSlotID
		END
	ELSE IF (SELECT COUNT(VaccinationSlotID) FROM deleted) > 0
		BEGIN
			UPDATE dbo.VaccinationSlot 
			SET Reserved = 0
			WHERE VaccinationSlotID=@VaccinationSlotID
		END
END


-------------------------------------------------------------------------------------------------------------------------------------------------
--4. Trigger to update the vaccinationDate in vaccinationBooking table 

GO
CREATE TRIGGER ut_UpdateVaccinationDate
ON dbo.VaccinationBooking 
FOR INSERT, UPDATE
AS
BEGIN
	DECLARE @VaccinationSlotID INT = 0;
	DECLARE @VaccinationSlotDateTime DATETIME;
	DECLARE @BookingID INT = 0;

	SELECT @VaccinationSlotID = VaccinationSlotID FROM inserted;
	SELECT @BookingID = BookingID FROM inserted ;
	SELECT @VaccinationSlotDateTime=VaccinationSlotDateTime FROM dbo.VaccinationSlot WHERE VaccinationSlotID = @VaccinationSlotID;

	UPDATE dbo.VaccinationBooking 
	SET VaccinationDate = @VaccinationSlotDateTime
	WHERE VaccinationSlotID=@VaccinationSlotID AND BookingID = @BookingID
END

-------------------------------------------------------------------------------------------------------------------------------------------------
-- 5. Calculate the PatientAge using the the PatientDateOfBirth.
	
/*	PatientAge - This column in Patient table is a computed column. Whenever Patient Date Of Birth is inserted in the table, 
	Patient Age will be computed by the system as while creating the PatientAge column, 
	we have provided : PatientAge AS DATEDIFF(hour,PatientDateOfBirth,GETDATE())/8766 
*/

-------------------------------------------------------------------------------------------------------------------------------------------------

	
