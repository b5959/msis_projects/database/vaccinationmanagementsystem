/*****************  DMDD_Group_4 INSERT STATEMENTS  **********/

USE "DMDD_Group_4";

--Delete the table data if it exists

DELETE FROM Orders;
DELETE FROM ManufacturerInventory;
DELETE FROM VaccineInventory;
DELETE FROM VaccinationCenterWarehouse;
DELETE FROM VaccinationBooking;
DELETE FROM VaccinationSlot;
DELETE FROM Vaccine;
DELETE FROM Manufacturer;
DELETE FROM Doctor;
DELETE FROM VaccinationCenter;
DELETE FROM Feedback;
DELETE FROM Disease;
DELETE FROM Patient;


--Insert Statements
INSERT INTO Patient
VALUES 
(320200, 'Sush', 'Rai', 'Sush@gmail.com', '1139102308', 'A+', 'Female', '1BMSN0900', '1991-03-22','United Health', 'UNIV1001', '32 ST', 'Seattle', 'WA', 'United States', '98101'),
(320201, 'Vaish', 'Arthm', 'Vaish@gmail.com', '2135102209', 'A-', 'Female', '1BNST0321', '1990-06-20','Humana', 'UNIV1002', '53 ST', 'Bellevue', 'WA', 'United States', '98005'),
(320202, 'Siri', 'Bhard', 'Siri@gmail.com', '3131102110', 'AB+', 'Female', '1BMSN0901', '1994-05-12','CVS Health', 'UNIV1003', '01 ST', 'Redmond', 'WA', 'United States', '98052'),
(320203, 'Chand', 'Losy', 'Chand@gmail.com', '4127102011', 'B-', 'Female', '1BNST0322', '1989-09-10', 'CIGNA', 'UNIV1004', '74 ST', 'Mercer', 'WA', 'United States', '54547'),
(320204, 'Timmy', 'Hilt', 'Timmy@gmail.com', '5123101912', 'O+', 'Male', '1BMSN0902', '1984-11-05', 'Humana', 'UNIV1005', '95 ST', 'San Diego', 'CA', 'United States', '95101'),
(320205, 'Sammy', 'Busy', 'Sammy@gmail.com', '6119101813', 'O-', 'Male', '1BNST0323','1987-12-02', 'CVS Health', 'UNIV1006', '01 ST', 'San Jose', 'CA', 'United States', '73923'),
(320206, 'Ralph', 'Lauren', 'Ralph@gmail.com', '7115101714', 'AB+', 'Male', '1BMSN0903','1991-10-12', 'United Health', 'UNIV1007', '116 ST', 'Los Angeles', 'CA', 'United States', '68978'),
(320207, 'Ross', 'Geller', 'Ross@gmail.com', '8111101615', 'B-', 'Male', '1BNST0324','2000-09-01', 'Humana', 'UNIV1008', '137 ST', 'Houston', 'TX', 'United States', '64032'),
(320208, 'Joey', 'Trib', 'Joey@gmail.com', '9107101516', 'A-', 'Male', '1BMSN0904','1984-04-02', 'CVS Health', 'UNIV1009', '01 ST', 'San Antonio', 'TX', 'United States', '59086'),
(320209, 'Monica', 'Xeor', 'Monica@gmail.com', '1010310141', 'AB+', 'Female', '1BNST0325','1997-05-10', 'CIGNA', 'UNIV1010', '158 ST', 'Dallas', 'TX', 'United States', '54140'),
(320210, 'Chandler', 'Bing', 'Chandler@gmail.com', '1109910131', 'O+', 'Male', '1BMSN0905','1985-09-06', 'United Health', 'UNIV1011', '179 ST', 'Austin', 'TX', 'United States', '49194');

INSERT INTO Disease
VALUES  
(1325,'Flu'),
(1326,'Dengue'),
(1327,'Chickenpox'),
(1328,'Mumps'),
(1329,'Covid'),
(1330,'Poliovirus'),
(1331,'Tetanus'),
(1332,'Hepatitis B'), 
(1333,'Hepatitis A'), 
(1334,'Rotavirus'),
(1335,'Whooping Cough');

INSERT INTO Feedback
VALUES
(200,	'Good service',	getdate(),	320200),
(201,	'Clean place',	getdate(),	320201),
(202,	'Satisfactory service',	getdate(),	320202),
(203,	'Well maintained',	getdate(),	320203),
(204,	'Excellent facility',	getdate(),	320204),
(205,	'Poor facility',	getdate(),	320205),
(206,	'Good service',	getdate(),	320206),
(207,	'Clean place',	getdate(),	320207),
(208,	'Good service',	getdate(),	320208),
(209,	'Poor facility',	getdate(),	320209);

INSERT INTO VaccinationCenter
VALUES  
(10010, 'Clinic', 'The Sunshine Practice' ,'32 ST', 'Seattle' ,'WA' ,'United States', '98101','1139102308'),
(10011, 'Hospital', 'Primary Care Family Practice' ,'53 ST', 'Bellevue' ,'WA' ,'United States', '98005','3131102110'),
(10012, 'Pharmacy', 'Go Medicine Care' ,'01 ST', 'Redmond' ,'WA' ,'United States', '98052','2135102209'),
(10013, 'Camps', 'Rainbow Family Practice' ,'74 ST', 'Mercer' ,'WA' ,'United States', '54547','3131102110'),
(10014, 'Camps', 'Go Clinic Family' ,'95 ST', 'San Diego' ,'CA' ,'United States', '95101','4127102011'),
(10015, 'Clinic', 'Rainbow General' ,'01 ST', 'San Jose' ,'CA' ,'United States', '73923','5123101912'),
(10016, 'Hospital', 'Go Support Clinic' ,'116 ST', 'Los Angeles' ,'CA' ,'United States', '68978','6119101813'),
(10017, 'Pharmacy', 'My Wellness Physician' ,'137 ST', 'Houston' ,'TX' ,'United States', '64032','7115101714'),
(10018, 'Hospital', 'Go Pro Family Center' ,'01 ST', 'San Antonio' ,'TX' ,'United States', '59086','8111101615'),
(10019, 'Pharmacy', 'Dallas Clinic' ,'158 ST', 'Dallas' ,'TX' ,'United States', '54140','9107101516'),
(10020, 'Camps', 'Trust Care Center' ,'179 ST', 'Austin' ,'TX' ,'United States', '49194','1010310141');


INSERT INTO Doctor
VALUES
(1001,'Sam','Nak','NC5300','Family physician','6510281324','sam@gmail.com',10010),
(1002,'Tam','Avi','NC5305','Emergency physician','7510581324','tam@gmail.com',10011),
(1003,'Kim','Cavale','NC5310','Pediatrician','8510881324','kim@gmail.com',10012),
(1004,'Lory','Ragesh','NC5315','Neurologist','9511181324','lory@gmail.com',10013),
(1005,'Jem','Rao','NC5320','Radiologists','1051148324','jem@gmail.com',10014),
(1006,'Kerry','Krish','NC5325','Emergency physician','1151178132','kerry@gmail.com',10015),
(1007,'Terry','Mooh','NC5330','Pediatrician','1251208132','terry@gmail.com',10016),
(1008,'Kourtney','Trim','NC5335','Neurologist','1351238132','trim@gmail.com',10017),
(1009,'Kendell','Denieal','NC5340','Radiologists','1451268132','kendell@gmail.com',10018),
(1010,'Page','Lary','NC5345','Family physician','1551298132','page@gmail.com',10019);

INSERT INTO Manufacturer 
VALUES
(500,	'Vaxchora',	'33 ST',	'Seattle',	'WA',	'United States'	,'98101'),
(501,	'BioThrax',	'13 ST',	'Bellevue',	'WA',	'United States'	,'98005'),
(502,	'Daptacel',	'01 ST',	'Redmond',	'WA',	'United States'	,'98052'),
(503,	'Infanrix',	'07 ST',	'Mercer',	'WA',	'United States'	,'54547'),
(504,	'ActHIB',	'27 ST',	'San Diego',	'CA',	'United States'	,'95101'),
(505,	'Hiberix',	'01 ST',	'SanJose',	'CA',	'United States'	,'73923'),
(506,	'PedvaxHIB',	'47 ST',	'LosAngeles',	'CA',	'United States'	,'68978'),
(507,	'Havrix',	'67 ST',	'Houston',	'TX',	'United States'	,'64032'),
(508,	'Vaqta',	'01 ST',	'SanAntonio',	'TX',	'United States'	,'59086'),
(509,	'Recombivax',	'87 ST',	'Dallas',	'TX',	'United States'	,'54140'),
(510,	'Shingrix',	'10 ST',	'Austin',	'TX',	'United States',	'49194');

INSERT INTO Vaccine
VALUES  
(201,'Flu Vaccine', 500,15,1325),
(202,'Dengue Vaccine',501,35,1326),
(203,'Chickenpox Vaccine',502,70,1327),
(204,'Mumps Vaccine',503,95,1328),
(205,'Covid Vaccine',504,122,1329),
(206,'Poliovirus Vaccine',505,150,1330),
(207,'Tetanus Vaccine',506,177,1331),
(208,'Hepatitis B Vaccine',507,215,1332),
(209,'Hepatitis A Vaccine',508,232,1333),
(210,'Rotavirus Vaccine',509,260,1334),
(211,'Whooping Cough Vaccine',510,287,1335);

-- O is not reserved / 1 is reserved
INSERT INTO VaccinationSlot
VALUES
(222,	getDate()+10,	10010,	1001,	0),
(223,	getDate()+11,	10011,	1002,	0),
(224,	getDate()+12,	10012,	1003,	0),
(225,	getDate()+13,	10013,	1004,	0),
(226,	getDate()+14,	10014,	1005,	0),
(227,	getDate()+15,	10015,	1006,	0),
(228,	getDate()+16,	10016,	1007,	0),
(229,	getDate()+17,	10013,	1008,	0),
(230,	getDate()+18,	10014,	1009,	0),
(231,	getDate()+19,	10015,	1010,	0),
(232,	getDate()+20,	10010,	1001,	0),
(233,	getDate()+21,	10010,	1001,	0),
(241,	getDate()+22,	10010,	1001,	0),
(242,	getDate()+23,	10010,	1001,	0),
(243,	getDate()+24,	10010,	1001,	0);

INSERT INTO VaccinationBooking
VALUES
(1,	320200, getdate(),	NULL,	222,	201),
(2,	320201,	getdate(),	NULL,	223,	202),
(3,	320202,	getdate(),	NULL,	224,	203),
(4,	320203,	getdate(),	NULL,	225,	204),
(5,	320204,	getdate(),	NULL,	226,	205),
(6,	320205,	getdate(),	NULL,	227,	203),
(7,	320206,	getdate(),	NULL,	228,	204),
(8,	320207,	getdate(),	NULL,	229,	205),
(9,	320208,	getdate(),	NULL,	230,	201),
(10,320209,	getdate(),	NULL,	231,	202),
(11,320201,	getdate(),	NULL,	232,	201),
(12,320200, getdate(),	NULL,	233,	202),
(13,320201, getdate(),	NULL,	241,	202),
(14,320200, getdate(),	NULL,	242,	202),
(15,320206, getdate(),	NULL,	243,	206);


INSERT INTO VaccinationCenterWarehouse
VALUES  
(20010, 10010, '32 ST' ,'Seattle' ,'WA', 'United States', '98101'),
(20011, 10011, '53 ST' ,'Bellevue' ,'WA', 'United States', '98005'),
(20012, 10012, '01 ST' ,'Redmond' ,'WA', 'United States', '98052'),
(20013, 10013, '74 ST' ,'Mercer' ,'WA', 'United States', '54547'),
(20014, 10014, '95 ST' ,'San Diego' ,'CA', 'United States', '95101'),
(20015, 10015, '01 ST' ,'San Jose' ,'CA', 'United States', '73923'),
(20016, 10016, '116 ST' ,'Los Angeles' ,'CA', 'United States', '68978'),
(20017, 10017, '137 ST' ,'Houston' ,'TX', 'United States', '64032'),
(20018, 10018, '01 ST' ,'San Antonio' ,'TX', 'United States', '59086'),
(20019, 10019, '158 ST' ,'Dallas' ,'TX', 'United States', '54140'),
(20020, 10020, '179 ST' ,'Austin' ,'TX', 'United States', '49194');


INSERT INTO VaccineInventory
VALUES  
(20010,	201,	530,	'2022-03-15'	,'2023-03-15',	3520),
(20011,	202,	531,	'2022-01-20'	,'2024-01-20',	2133),
(20012,	203,	532,	'2021-11-27'	,'2024-11-26',	4520),
(20013,	204,	533,	'2021-10-04'	,'2025-10-03',	5230),
(20014,	205,	534,	'2021-08-11'	,'2026-08-10',	5730),
(20015,	206,	535,	'2021-06-18'	,'2027-06-17',	6481),
(20016,	207,	536,	'2021-04-25'	,'2028-04-23',	7233),
(20017,	208,	537,	'2021-03-02'	,'2029-02-28',	7985),
(20018,	209,	538,	'2021-01-07'	,'2030-01-05',	8736),
(20019,	210,	539,	'2022-11-14'	,'2030-11-12',	9488),
(20020,	211,	540,	'2022-09-21'	,'2031-09-19',	1024),
(20011,	202,	541,	'2021-01-01'	,'2022-12-25',	2514),
(20011,	202,	542,	'2020-08-21'	,'2022-12-20',	0536),
(20013,	203,	543,	'2020-07-25'	,'2022-12-22',	0421);

INSERT INTO ManufacturerInventory
VALUES
(1100,	201,	11850,	500),
(1101,	202,	23000,	501),
(1102,	203,	21000,	502),
(1103,	204,	27666,	503),
(1104,	205,	32166,	504),
(1105,	206,	36666,	505),
(1106,	207,	41166,	506),
(1107,	208,	45666,	507),
(1108,	209,	50166,	508),
(1109,	210,	54666,	509),
(1110,	211,	59166,	510);


INSERT INTO Orders
VALUES
(2100,    '2022-05-01',   '2022-05-20',        345,    10020,    211,    508,    1001),
(2101,    '2022-06-11',   '2022-06-20',        530,    10015,    207,    507,    1001),
(2102,    '2022-09-30',   '2022-10-06',        920,    10018,    203,    509,    1001),
(2103,       getdate(),    getdate()+5,        115,    10010,    201,    500,    1001),
(2104,       getdate(),    getdate()+7,        250,    10011,    202,    501,    1001),
(2105,       getdate(),    getdate()+8,        215,    10012,    203,    502,    1001),
(2106,       getdate(),    getdate()+3,        900,    10013,    204,    503,    1001),
(2107,       getdate(),    getdate()+3,        350,    10014,    205,    504,    1001),
(2108,       getdate(),    getdate()+5,       1000,    10015,    206,    505,    1001),
(2109,       getdate(),    getdate()+6,        850,    10016,    207,    506,    1001),
(2110,       getdate(),    getdate()+1,        900,    10017,    208,    507,    1001),
(2111,       getdate(),    getdate()+8,        950,    10018,    209,    508,    1001),
(2112,       getdate(),    getdate()+4,       1000,    10019,    210,    509,    1001),
(2113,       getdate(),    getdate()+9,        213,    10020,    211,    510,    1001);

-------------------------------------------------------------------------------------------------------------------------------
