/****************************  Column Level Encryption  *****************************/

/*
	1) We have implemented Column Encryption for two columns :
	-- PatientSSN column in Patient table
	-- Doctor Registration Number in Doctor table

	2) The Check commands for encryption and HouseKeeping commands have been provided in below file itself
*/

-- 1) Column encryption on Patient SSN number

USE "DMDD_Group_4";

-- Create DMK
		CREATE MASTER KEY
		ENCRYPTION BY PASSWORD = 'Password@Grp4';

-- Create certificate to protect symmetric key
		CREATE CERTIFICATE VMPATIENTSSNCERTIFICATE
		WITH SUBJECT = 'VM DB SSN Certificate',
		EXPIRY_DATE = '2023-01-01';

-- Create symmetric key to encrypt data
		CREATE SYMMETRIC KEY SSNSymmetricKey
		WITH ALGORITHM = AES_128
		ENCRYPTION BY CERTIFICATE VMPATIENTSSNCERTIFICATE;

		ALTER TABLE Patient  
		ADD EncryptedPatientSSN varbinary(128);  

-- Open symmetric key
		OPEN SYMMETRIC KEY SSNSymmetricKey
		DECRYPTION BY CERTIFICATE VMPATIENTSSNCERTIFICATE;

-- Encrypt the value in column Patient SSN with symmetric   
-- key - SSNSymmetricKey. Save the result in column EncryptedPatientSSN.
		UPDATE Patient
		SET EncryptedPatientSSN = EncryptByKey(Key_GUID('SSNSymmetricKey'), PatientSSN);

		SELECT * FROM Patient;

-- Verifying the encryption.  
-- First, open the symmetric key with which to decrypt the data. 
		OPEN SYMMETRIC KEY SSNSymmetricKey  
		DECRYPTION BY CERTIFICATE VMPATIENTSSNCERTIFICATE;  

-- Fetching the original PatientSSN, the encrypted Patient SSN, and the   
-- decrypted ciphertext. If the decryption worked, the original  
-- and the decrypted PatientSSN will match.

		SELECT PatientSSN,EncryptedPatientSSN AS 'Encrypted Patient SSN',  
		CONVERT(varchar, DecryptByKey(EncryptedPatientSSN))   
		AS 'Decrypted Patient SSN'  
		FROM Patient; 

/***************************************HOUSEKEEPING*********************************************************/

-- Now in this step we will DROP SSN Column as Encrypted SSN Column is present. But as this was for demo purpose we will be dropping the encrypted column
		Alter Table Patient
		DROP COLUMN EncryptedPatientSSN

-- Close the symmetric key
		CLOSE SYMMETRIC KEY SSNSymmetricKey;
-- Drop the symmetric key
		DROP SYMMETRIC KEY SSNSymmetricKey;
-- Drop the certificate
		DROP CERTIFICATE VMPATIENTSSNCERTIFICATE;
-- Drop the DMK
		DROP MASTER KEY;

----------------------------------------------------------------------------------------------------------------------------------

-- 1) Column encryption on Doctor Registration number

USE "DMDD_Group_4";

-- Create DMK
		CREATE MASTER KEY
		ENCRYPTION BY PASSWORD = 'Password@Grp4';

-- Create certificate to protect symmetric key
		CREATE CERTIFICATE VMDOCTORREGISTRATIONCERTIFICATE
		WITH SUBJECT = 'VM DB Doctor Registration Certificate',
		EXPIRY_DATE = '2023-01-01';

-- Create symmetric key to encrypt data
		CREATE SYMMETRIC KEY RegistrationSymmetricKey
		WITH ALGORITHM = AES_128
		ENCRYPTION BY CERTIFICATE VMDOCTORREGISTRATIONCERTIFICATE;

		ALTER TABLE Doctor  
		ADD EncryptedDoctorRegistrationNumber varbinary(128);  

-- Open symmetric key
		OPEN SYMMETRIC KEY RegistrationSymmetricKey
		DECRYPTION BY CERTIFICATE VMDOCTORREGISTRATIONCERTIFICATE;

-- Encrypt the value in column DoctorRegistrationNumber with symmetric   
-- key RegistrationSymmetricKey. Save the result in column EncryptedDoctorRegistrationNumber.
		UPDATE Doctor
		SET EncryptedDoctorRegistrationNumber = EncryptByKey(Key_GUID('RegistrationSymmetricKey'), DoctorRegistrationNumber);

		SELECT * FROM Doctor;

-- Verifying the encryption.  
-- First, open the symmetric key with which to decrypt the data. 
		OPEN SYMMETRIC KEY RegistrationSymmetricKey  
		DECRYPTION BY CERTIFICATE VMDOCTORREGISTRATIONCERTIFICATE;  

-- Fetching the original Doctor Registration Number, the encrypted Registration Number, and the   
-- decrypted ciphertext. If the decryption worked, the original  
-- and the decrypted Doctor Registration Number will match.

		SELECT DoctorRegistrationNumber,EncryptedDoctorRegistrationNumber AS 'Encrypted Doctor Registration Number',  
		CONVERT(varchar, DecryptByKey(EncryptedDoctorRegistrationNumber))   
		AS 'Decrypted Doctor Registration Number'  
		FROM Doctor; 

/***************************************HOUSEKEEPING*********************************************************/


-- Now in this step we will DROP DoctorRegistrationNumber Column as Encrypted Registration Column is present. But as this was for demo purpose we will be dropping the encrypted column
		Alter Table Doctor
		DROP COLUMN EncryptedDoctorRegistrationNumber

-- Close the symmetric key
		CLOSE SYMMETRIC KEY RegistrationSymmetricKey;
-- Drop the symmetric key
		DROP SYMMETRIC KEY RegistrationSymmetricKey;
-- Drop the certificate
		DROP CERTIFICATE VMDOCTORREGISTRATIONCERTIFICATE;
-- Drop the DMK
		DROP MASTER KEY;

-----------------------------------------------------------------------------------------------------------------------------