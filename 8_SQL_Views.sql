/********************** VIEWS *************************/

-- 1) View to provide information regarding the Most Booked Vaccine per Vaccination Centers

USE "DMDD_Group_4";
GO

CREATE VIEW dbo.vw_MostBookedVaccinePerVaccinationCenter
AS
WITH CTE AS
(
SELECT VC.VaccinationCenterID, VB.VaccineID
FROM VaccinationSlot VS
INNER JOIN VaccinationCenter VC ON VC.VaccinationCenterID = VS.VaccinationCenterID
INNER JOIN VaccinationBooking VB ON VB.VaccinationSlotID = VS.VaccinationSlotID
)
SELECT Temp.VaccinationCenterID AS [Vaccination Center ID], VC.VaccinationCenterName AS [Vaccination Center Name], 
Temp.VaccineID AS [Vaccine ID], V.VaccineName AS [Vaccine Name], Temp.VaccineBookingCount AS [Total Bookings for Vaccine]
FROM 
	(SELECT VaccinationCenterID, VaccineID, COUNT(VaccineID) AS [VaccineBookingCount], 
	RANK() OVER(PARTITION BY VaccinationCenterID ORDER BY COUNT(VaccineID) DESC
	) AS [R]
FROM CTE
GROUP BY VaccinationCenterID, VaccineID) AS Temp
INNER JOIN VaccinationCenter VC ON Temp.VaccinationCenterID = VC.VaccinationCenterID
INNER JOIN Vaccine V ON Temp.VaccineID = V.VaccineID
WHERE R <= 2
ORDER BY Temp.VaccinationCenterID
OFFSET 0 ROWS;

GO

--------------------------------------------------------------------------------------------------------------------------------

-- 2) View to provide information regarding Slots Availability and Utilization

GO
CREATE VIEW dbo.vw_VaccinationSlotsAvailabilityAndUtilization
AS
WITH CTE AS
(
	SELECT VS.VaccinationCenterID AS [Vaccination Center ID], VC.VaccinationCenterName AS [Vaccination Center Name],
	VC.VaccinationCenterType AS [Vaccination Center Type],Count(VaccinationSlotID) AS [Total Available Slots],
			SUM(CASE WHEN Reserved = 1 THEN 1 ELSE 0 END) AS [Reserved Slot Count],
			SUM(CASE WHEN Reserved = 0 THEN 1 ELSE 0 END) AS [Unserved Slot Count]
	From VaccinationSlot VS
	JOIN VaccinationCenter VC ON VS.VaccinationCenterID = VC.VaccinationCenterID
	GROUP BY VS.VaccinationCenterID, VC.VaccinationCenterName,VC.VaccinationCenterType
)
SELECT *, CAST((CAST([Reserved Slot Count] as float))/(CAST([Total Available Slots] AS FLOAT))*100 AS VARCHAR)+' %' AS [Slot Utilization Percent] 
FROM CTE
ORDER BY [Vaccination Center ID]
OFFSET 0 ROWS;
GO


--------------------------------------------------------------------------------------------------------------------------------

-- 3) View to provide information regarding vaccines which will be expiring within one month

GO
CREATE VIEW dbo.vw_VaccineExpiryStatusFromInventory
AS
SELECT V.VaccineID AS [Vaccine ID], V.VaccineName AS [ Vaccine Name], VI.BatchID AS [Vaccine Batch ID],
VCW.VaccinationCenterWarehouseID AS [Vaccination Center Warehouse ID], VC.VaccinationCenterName AS [Vaccination Center Name], 
VC.VaccinationCenterType AS [Vaccination Center Type], CONCAT(VC.VaccinationCenterStreet,', ', 
VC.VaccinationCenterCity,', ', VC.VaccinationCenterState,', ',
VC.VaccinationCenterCountry,', ',VC.VaccinationCenterZipCode) AS [Vaccination Center Address]
FROM 
Vaccine V
JOIN
VaccineInventory VI ON V.VaccineID = VI.VaccineID
JOIN
VaccinationCenterWarehouse VCW ON VI.VaccinationCenterWarehouseID = VCW.VaccinationCenterWarehouseID
JOIN VaccinationCenter VC ON VCW.VaccinationCenterID = VC.VaccinationCenterID
WHERE VI.VaccineExpiryDate <= GETDATE() + 30
GO


--------------------------------------------------------------------------------------------------------------------------------

-- 4) View to provide information regarding Vaccine Orders placed by Vaccination Center with Manufacturer
GO
CREATE VIEW dbo.vw_OrderStatus
AS
SELECT VC.VaccinationCenterName AS [Vaccination Center Name], V.VaccineName AS [Vaccine Name], O.OrderQuantity AS [Vaccine Order Quantity], 
			M.ManufacturerName AS [Manufacturer Name],  CAST(O.OrderDate AS DATE) AS [Order Date], 
			O.OrderStatusID AS [Order Status ID], OS.OrderStatus AS [Order Status] , CAST(O.OrderDeliveryDate AS DATE) AS [Order Delivery Date]
FROM
Orders O
JOIN OrderStatuses OS 
ON O.OrderStatusID = OS.OrderStatusID
LEFT JOIN VaccinationCenter VC
ON O.VaccinationCenterID = VC.VaccinationCenterID
LEFT JOIN Vaccine V
ON O.VaccineID = V.VaccineID
LEFT JOIN Manufacturer M
ON O.ManufacturerID = M.ManufacturerID;
GO


---------------------------------------------------------------------------------------------------------------------------









