/*********************** HOUSEKEEPING SCRIPT FOR ALL VIEWS / TABLE LEVEL CONSTRAINTS / TRIGGERS / FUNCTIONS / TABLE DATA / TABLES********************/

USE "DMDD_Group_4";

/************************** Dropping Views********************************************/
-- HouseKeeping for dbo.vw_MostBookedVaccinePerVaccinationCenter (View Code in file : SQL_Views.sql)

	DROP VIEW dbo.vw_MostBookedVaccinePerVaccinationCenter

-- HouseKeeping for dbo.vw_VaccinationSlotsAvailabilityAndUtilization (View Code in file : SQL_Views.sql)
	
	DROP VIEW dbo.vw_VaccinationSlotsAvailabilityAndUtilization 

-- HouseKeeping for dbo.vw_VaccineExpiryStatusFromInventory (View Code in file : SQL_Views.sql)

	DROP VIEW dbo.vw_VaccineExpiryStatusFromInventory 

-- HouseKeeping for dbo.vw_OrderStatus (View Code in file : SQL_Views.sql)
 
	DROP VIEW dbo.vw_OrderStatus;


/************************** Dropping Triggers And Constraint********************************************/
-- HOUSEKEEPING for Trigger - bookingDateCheck (Trigger Code in file : SQL_TableLevelConstraintAndTriggers.sql)
 
	DROP TRIGGER bookingDateCheck;

-- HOUSEKEEPING for Trigger - appoinmentDateCheck (Trigger Code in file : SQL_TableLevelConstraintAndTriggers.sql)

	DROP TRIGGER appoinmentDateCheck;

-- HOUSEKEEPING for Constraint - checkForVaccineSlot (Trigger Code in file : SQL_TableLevelConstraintAndTriggers.sql)

	ALTER TABLE VaccinationBooking DROP CONSTRAINT checkForVaccineSlot
	DROP FUNCTION checkIfVacctionSlotIsReserved;

-- HOUSEKEEPING for Constraint - checkForVaccCount (Trigger Code in file : SQL_TableLevelConstraintAndTriggers.sql
    
	ALTER TABLE dbo.VaccinationBooking DROP CONSTRAINT checkForVaccCount 
	DROP FUNCTION checkVaccineAvailability
	DROP FUNCTION getVaccineCenterId
	DROP FUNCTION getWarehouseId

-- HOUSEKEEPING for Constraint - orderQuantityCheckConstraint (Trigger Code in file : SQL_TableLevelConstraintAndTriggers.sql)

	ALTER TABLE dbo.Orders DROP CONSTRAINT orderQuantityCheckConstraint;
	DROP FUNCTION orderQuantityCheck;


/************************** Triggers related Computed Columns ********************************************/

-- HOUSEKEEPING for TRIGGER ut_vaccinationCountDec (Trigger Code in file : SQL_ComputedColumns.sql)

	DELETE FROM VaccinationBooking WHERE BookingID=91
	DELETE FROM VaccinationSlot WHERE VaccinationSlotID=291

-- HOUSEKEEPING for	TRIGGER ut_vaccinationQuantityManagement (Trigger Code in file : SQL_ComputedColumns.sql)

	DELETE FROM Orders WHERE OrderID=2199

-- HOUSEKEEPING for Trigger to book/cancel the vaccination slot using the reserved column in the vaccinationSlot table (Trigger Code in file : SQL_ComputedColumns.sql)

	DELETE FROM VaccinationBooking WHERE BookingID=91
	DELETE FROM VaccinationSlot WHERE VaccinationSlotID=291

-- HOUSEKEEPING for Trigger to update the vaccinationDate in vaccinationBooking table  (Trigger Code in file : SQL_ComputedColumns.sql)
	
	DELETE FROM VaccinationBooking WHERE BookingID=91
	DELETE FROM VaccinationSlot WHERE VaccinationSlotID=291



/************************** DELETING TABLES ********************************************/

	DELETE FROM Orders;
	DELETE FROM OrderStatuses;
	DELETE FROM ManufacturerInventory;
	DELETE FROM VaccineInventory;
	DELETE FROM VaccinationCenterWarehouse;
	DELETE FROM VaccinationBooking;
	DELETE FROM VaccinationSlot;
	DELETE FROM Vaccine;
	DELETE FROM Manufacturer;
	DELETE FROM Doctor;
	DELETE FROM VaccinationCenter;
	DELETE FROM Feedback;
	DELETE FROM Disease;
	DELETE FROM Patient;
	

/************************** DROPPING TABLES ********************************************/

	DROP TABLE IF Exists Orders;
	DROP TABLE IF Exists OrderStatuses;
	DROP TABLE IF Exists ManufacturerInventory;
	DROP TABLE IF Exists VaccineInventory;
	DROP TABLE IF Exists VaccinationCenterWarehouse;
	DROP TABLE IF Exists VaccinationBooking;
	DROP TABLE IF Exists VaccinationSlot;
	DROP TABLE IF Exists Vaccine;
	DROP TABLE IF Exists Manufacturer;
	DROP TABLE IF Exists Doctor;
	DROP TABLE IF Exists VaccinationCenter;
	DROP TABLE IF Exists Feedback;
	DROP TABLE IF Exists Disease;
	DROP TABLE IF Exists Patient;