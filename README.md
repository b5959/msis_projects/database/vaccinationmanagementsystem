**VACCINATION MANAGEMENT SYSTEM**



**MISION AND PURPOSE**

The vaccination management system is an application to facilitate easy access to get vaccinated against any health problem. This solution provides a unified experience with a centralized vaccination management system that integrates patients, vaccination centers, vaccination slot booking, vaccine administration, and distribution. 


--------------------------------------------------------------------------------------------------------------------------------------------------------


**BUSINESS PROBLEMS ADDRESSED**

•	Allow patients to book vaccination appointments on a given date and location

•	Allow vaccination centers to record patient appointments

•	Allow vaccination centers to track the available vaccines in their warehouse

•	Allow vaccination centers to place order to manufacturers if the vaccines are low in stock at their warehouse

•	Allow manufacturers to process or reject the orders received

•	Allow manufacturers to track the available vaccines in their inventory

•	Use feedback provided by the patients for data analytics.


--------------------------------------------------------------------------------------------------------------------------------------------------------


**PROJECT FILE SUMMARY & ORDER OF EXECUTION**

NOTE: Please Execute the files in the order provided below

**1. Creation of Tables – SQL_DBCreateStatements.sql**

**2. Table-level CHECK Constraints based on Functions and Triggers - SQL_TableLevelConstraintAndTriggers.sql**

a. Avoiding a patient booking an already occupied/reserved vaccination slot

b. Check if the requested vaccine is available in the vaccination center prior to booking

c. Check if the order can be fulfilled with the requested number of vaccines from the manufacturer

d. Restricting a patient to book a vaccination slot for any previous dates

e. Restricting a patient to book a vaccination slot for any future dates after 30 days from the current date

f. Avoiding a patient booking more than one vaccination appointment on the same day

**3. Computed Columns based on a function – SQL_ComputedColumns.sql**

a. Calculating the age of the patient based on the Date of Birth provided

b. Decrement the vaccine count in the inventory once there is a vaccination booking recorded

c. Increment the Vaccine count in the inventory once the order is delivered from the manufacturer

d. Book or cancel the vaccination slot using the reserved column in the vaccination slot table

**4. Insertion of data - SQL_DBInsertStatements.sql**

**5. Column Data Encryption - SQL_ColumnEncryption.sql**

a. Encrypting Patient sensitive information –SSN

b. Encrypting Doctor sensitive information – Registration Number

**6. Views - SQL_Views.sql**

a. To provide information regarding the Most Booked Vaccine at a Vaccination Center

b. To provide information regarding Vaccination Slots Availability and Utilization

c. To provide information regarding the vaccines that will be expiring within one month

d. To provide information regarding Vaccine Orders placed by Vaccination Center with the Manufacturer

**7. Housekeeping – SQL_HouseKeeping.sql**

**8. SQL_TestRun.sql – All test cases and commands are present in this file**
   
