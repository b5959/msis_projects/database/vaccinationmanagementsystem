/**************************   VERIFICATION COMMANDS ***********************/

USE "DMDD_Group_4";

/**************************   VERIFICATION COMMANDS FOR TRIGGERS AND TABLE LEVEL CONSTRAINTS ***********************/
-- 1) - 2) Test Cases for Trigger - bookingDateCheck (Trigger Code in file : SQL_TableLevelConstraintAndTriggers.sql)

	-- Test Cases
	-- This should fail as the Vaccination Date is a previous date:
 		INSERT INTO VaccinationBooking VALUES (91,    320200, getdate(),    '2022-11-30',    232,    201);
 
	-- This should fail as the Vaccination Date is after 30 days:
 		INSERT INTO VaccinationBooking VALUES (100,    320200, getdate(),    getDate()+35,    232,    201);


---------------------------------------------------------------------------------------------------------------------------

-- 3) Test Cases for Trigger - appoinmentDateCheck (Trigger Code in file : SQL_TableLevelConstraintAndTriggers.sql)

	-- Test Case
	-- This should fail as a patient is booking two vaccine slots on a same day:
		INSERT INTO VaccinationBooking VALUES (100,    320200, getdate(),    '2022-12-02',    241,    201);
 
 ---------------------------------------------------------------------------------------------------------------------------

 -- 4) Test Cases for Constraint - checkForVaccineSlot (Trigger Code in file : SQL_TableLevelConstraintAndTriggers.sql)

	-- Test Case
	-- This should fail as there is already a Vaccination Slot Id - 222 in VaccinationSlot Table
		INSERT INTO VaccinationBooking VALUES (100,    320200, getdate(),    '2022-12-15',    222,    201);

 ---------------------------------------------------------------------------------------------------------------------------

 -- 5) Test Cases for Constraint - checkForVaccCount (Trigger Code in file : SQL_TableLevelConstraintAndTriggers.sql)

	-- Test Case
	-- The below scenario will fail as we are setting the vaccine count to 0 and the patient is trying to book for the same vaccine that has count 0
		UPDATE dbo.VaccineInventory SET VaccineCount = 0 WHERE VaccinationCenterWarehouseID = 20010 AND VaccineID = 201

		INSERT INTO VaccinationBooking VALUES(700,	320200,	getdate(),	'2022-12-06',	243	, 201)

	-- Reverting VaccineCount back to the original value
		UPDATE dbo.VaccineInventory SET VaccineCount = 2231 WHERE VaccinationCenterWarehouseID = 20010 AND VaccineID = 201

 ---------------------------------------------------------------------------------------------------------------------------

 -- 6) Test Cases for Constraint - orderQuantityCheckConstraint (Trigger Code in file : SQL_TableLevelConstraintAndTriggers.sql)

	-- Test Case
	-- The below scenario will fail with the UPDATE command
		 INSERT INTO Orders VALUES(2103,'2022-05-01', '2022-06-01', 12100, 10010, 201, 500, 1001);

		 SELECT * FROM Orders WHERE OrderID = 2103

		 SELECT * FROM ManufacturerInventory WHERE VaccineID = 201 AND ManufacturerID = 500

		 UPDATE dbo.Orders SET OrderStatusID = 1003 WHERE OrderID = 2103

	-- Deleting the order created in above test case
		DELETE FROM Orders WHERE OrderID IN (2103)
 
  ---------------------------------------------------------------------------------------------------------------------------

  /**************************   VERIFICATION COMMANDS FOR TRIGGERS RELATED TO COMPUTED COLUMNS ***********************/

 -- 7) Test Cases for TRIGGER ut_vaccinationCountDec (Trigger Code in file : SQL_ComputedColumns.sql)

	-- Test Case

	--VaccinationSlot=291
	--DoctorID=1001
	--VaccinationCenterId=10015
	--PatientID=320200
	--VaccineID=206
	--VaccinationCenterWareHouseID=20015

	--get the current vaccine count for VaccinationCenterWarehouseID=20015  AND VaccineID=206
		SELECT VaccineCount FROM dbo.VaccineInventory WHERE VaccinationCenterWarehouseID=20015  AND VaccineID=206;
	
	-- Insert a vaccinationSlot with VaccinationSlotId = 291 DoctorID 1001 VaccinationCenterId = 10015
		INSERT INTO VaccinationSlot VALUES (291,  '2022-12-30',    10015,    1001,    0);
		INSERT INTO VaccinationBooking VALUES (91,    320206, getdate(),  null,    291,    206);
	
	--Check if the vaccine count is decremented by 1 for VaccinationCenterWarehouseID=20015  AND VaccineID=206
		SELECT VaccineCount FROM dbo.VaccineInventory WHERE VaccinationCenterWarehouseID=20015  AND VaccineID=206;


-------------------------------------------------------------------------------------------------------------------------

-- 8) Test Case for	TRIGGER ut_vaccinationQuantityManagement (Trigger Code in file : SQL_ComputedColumns.sql)

	--Test Cases
	--VaccinationCenterId=10018
	--VaccineID=209
	--VaccinationCenterWareHouseID=20018
	--ManufacturerID=508
	--OrderID=2199

	--get the current vaccine count for VaccinationCenterWarehouseID=20015  AND VaccineID=206
		SELECT VaccineCount FROM dbo.VaccineInventory WHERE VaccinationCenterWarehouseID=20018  AND VaccineID=209;
		SELECT VaccineQuantity FROM dbo.ManufacturerInventory WHERE ManufacturerID=508  AND VaccineID=209;

	-- Insert an order with ID=2199 orderQuantity=300, VaccinationCenterId=10018, ManufacturerID=508
		INSERT INTO Orders VALUES (2199, getdate(),   getdate() + 30, 300,    10018,    209,    508,    1001);

	--Check the values in VaccineInventory and ManufacturerInventory as it shouldn't have changed 
	--because the order status is in received/processing
		SELECT VaccineCount FROM dbo.VaccineInventory WHERE VaccinationCenterWarehouseID=20018  AND VaccineID=209;
		SELECT VaccineQuantity FROM dbo.ManufacturerInventory WHERE ManufacturerID=508  AND VaccineID=209;
	
	-- Update the order status to DELIVERED=1003
		UPDATE dbo.Orders SET OrderStatusID = 1003 WHERE OrderID=2199;
	
	--Check the values in VaccineInventory and ManufacturerInventory as it should have changed 
		SELECT VaccineCount FROM dbo.VaccineInventory WHERE VaccinationCenterWarehouseID=20018  AND VaccineID=209;
		SELECT VaccineQuantity FROM dbo.ManufacturerInventory WHERE ManufacturerID=508  AND VaccineID=209;
	
-------------------------------------------------------------------------------------------------------------------------

-- 9) Test Cases for Trigger to book/cancel the vaccination slot using the reserved column in the vaccinationSlot table (Trigger Code in file : SQL_ComputedColumns.sql)

	--Test Cases

	--VaccinationSlot=291
	--DoctorID=1001
	--VaccinationCenterId=10015
	--PatientID=320200
	--VaccineID=206
	
	-- Insert a vaccinationSlot that is not reserved. Using VaccinationSlotId = 291 DoctorID 1001 VaccinationCenterId = 10015
		INSERT INTO VaccinationSlot VALUES (298,  '2022-12-31',    10015,    1001,    0);

	--Check if the reserved value is O
		SELECT VaccinationSlotID, Reserved FROM dbo.VaccinationSlot WHERE VaccinationSlotID=298;
	
	-- Insert a vaccination booking that uses the above vaccination slot
		INSERT INTO VaccinationBooking VALUES (110,    320206, getdate(),  null,    298,    206);
	
	--Check if the reserved value is updated to 1
		SELECT VaccinationSlotID, Reserved FROM dbo.VaccinationSlot WHERE VaccinationSlotID=298;
	
-------------------------------------------------------------------------------------------------------------------------

-- 10) Test Cases for Trigger to update the vaccinationDate in vaccinationBooking table  (Trigger Code in file : SQL_ComputedColumns.sql)
	
	--Test Cases

	--VaccinationSlot=291
	--DoctorID=1001
	--VaccinationCenterId=10015
	--PatientID=320200
	--VaccineID=206

	-- Insert a vaccinationSlot that is not reserved. Using VaccinationSlotId = 291 DoctorID 1001 VaccinationCenterId = 10015
		INSERT INTO VaccinationSlot VALUES (249,  getdate() + 2,    10015,    1001,    0);
	
	--Get the VaccinationSlotDateTime
		SELECT VaccinationSlotDateTime FROM dbo.VaccinationSlot WHERE VaccinationSlotID=249;
	
	-- Insert a vaccination booking that uses the above vaccination slot and set the VaccinationDate = NULL
		INSERT INTO VaccinationBooking VALUES (111,    320206, getdate(),  NULL,    249,    206);
	
	--Check if the VaccinationDate value is equal to the VaccinationSlotDateTime value
		SELECT VaccinationDate FROM dbo.VaccinationBooking WHERE BookingID =111;
	
-------------------------------------------------------------------------------------------------------------------------

-- 11) Test Cases for Computed Column PatientAge in Patient Table

		SELECT PatientDateOfBirth, PatientAge FROM Patient



 /**************************   VERIFICATION COMMANDS FOR VIEWS ***********************/

 -- Checking the view dbo.vw_MostBookedVaccinePerVaccinationCenter (View Code in file : SQL_Views.sql)

	SELECT * FROM dbo.vw_MostBookedVaccinePerVaccinationCenter

-- Checking the view vw_VaccinationSlotsAvailabilityAndUtilization (View Code in file : SQL_Views.sql)

	SELECT * FROM dbo.vw_VaccinationSlotsAvailabilityAndUtilization

-- Checking the view vw_VaccineExpiryStatusFromInventory (View Code in file : SQL_Views.sql)

	SELECT * FROM dbo.vw_VaccineExpiryStatusFromInventory

-- Checking the view vw_OrderStatus (View Code in file : SQL_Views.sql)

	SELECT * FROM dbo.vw_OrderStatus;



